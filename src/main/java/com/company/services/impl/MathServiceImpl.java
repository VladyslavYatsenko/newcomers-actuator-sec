package com.company.services.impl;

import com.company.services.MathService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class MathServiceImpl implements MathService {
    @Override
    public double add(double a, double b) {
        log.info("Adding two numbers {} , {}", a, b);
        return a + b;
    }

    @Override
    public double remove(double a, double b) {
        log.info("Taking away two numbers {} , {}", a, b);
        return a - b;
    }

    @Override
    public double multiply(double a, double b) {
        log.info("Multiplying away two numbers {} , {}", a, b);
        return a * b;
    }

    @Override
    public double division(double a, double b) {
        log.info("Dividing away two numbers {} , {}", a, b);
        return a / b;
    }
}
