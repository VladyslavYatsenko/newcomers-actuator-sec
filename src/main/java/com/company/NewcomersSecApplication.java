package com.company;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewcomersSecApplication {

	public static void main(String[] args) {
		SpringApplication.run(NewcomersSecApplication.class, args);
	}

}
