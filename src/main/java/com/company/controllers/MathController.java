package com.company.controllers;

import com.company.services.MathService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class MathController {
    private MathService mathService;

    @GetMapping("/add/{a}/{b}")
    public double add(@PathVariable(value = "a") double a,
                      @PathVariable(value = "b") double b) {
        return mathService.add(a, b);

    }

    @GetMapping("/remove/{a}/{b}")
    public double remove(@PathVariable(value = "a") double a,
                         @PathVariable(value = "b") double b) {
        return mathService.remove(a, b);

    }

    @GetMapping("/multiply/{a}/{b}")
    public double multiply(@PathVariable(value = "a") double a,
                           @PathVariable(value = "b") double b) {
        return mathService.multiply(a, b);

    }

    @GetMapping("/divide/{a}/{b}")
    public double divide(@PathVariable(value = "a") double a,
                         @PathVariable(value = "b") double b) {
        return mathService.division(a, b);

    }
}
