package com.company.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.security.PermitAll;

@Controller
@PermitAll
@Slf4j
public class MainController {

    @GetMapping("/")
    public String index() {
        log.info("Opened Index Page");
        log.info("Principal " + SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
        return "index";
    }

}

